<?php

namespace Drupal\entity_display_mode\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Define the default formatter for the entity display mode field.
 *
 * @FieldFormatter(
 *   id = "entity_display_mode_default",
 *   label = @Translation("Visible"),
 *   description = @Translation("Display the display mode value."),
 *   field_types = {
 *     "entity_display_mode"
 *   }
 * )
 */
class EntityDisplayModeDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'inline_template',
        '#template' => '{{ value|nl2br }}',
        '#context' => ['value' => $item->value],
      ];
    }

    return $elements;
  }

}
