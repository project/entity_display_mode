<?php

namespace Drupal\entity_display_mode\Plugin\Field\FieldType;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Define the entity display mode field item.
 *
 * @FieldType(
 *   id = "entity_display_mode",
 *   label = @Translation("Display mode"),
 *   description = @Translation("Collect entity display mode."),
 *   default_widget = "entity_display_mode_select",
 *   default_formatter = "entity_display_mode_default"
 * )
 */
class EntityDisplayModeFieldItem extends FieldItemBase {

  /**
   * {@inheritDoc}
   */
  public static function defaultFieldSettings() {
    return [
      'display_type' => 'form',
      'display_mode' => [],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ]
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel('Display mode');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritDoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);
    $form['#prefix'] = '<div id="entity-display-mode-field-settings">';
    $form['#suffix'] = '</div>';

    $display_type = $this->getFormStateValue('display_type', $form_state);

    $form['display_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Display Type'),
      '#description' => $this->t('Select the display mode type.'),
      '#required' => TRUE,
      '#options' => [
        'view' => $this->t('View'),
        'form' => $this->t('Form'),
      ],
      '#ajax' => [
        'event' => 'change',
        'method' => 'replace',
        'wrapper' => 'entity-display-mode-field-settings',
        'callback' => [$this, 'replaceAjaxCallback']
      ],
      '#default_value' => $display_type
    ];

    if (isset($display_type) && !empty($display_type)) {
      $form['display_mode'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Display Modes'),
        '#description' => $this->t('Select the display modes that should be 
          shown. <br/> <strong>Note:</strong> If none are selected then all 
          display modes will be displayed.'
        ),
        '#options' => $this->getEntityDisplayModeOptions($display_type),
        '#default_value' => $this->getFormStateValue('display_mode', $form_state, [])
      ];
    }

    return $form;
  }

  /**
   * Replace ajax submission callback.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   *
   * @return array
   *   The ajax form elements.
   */
  public function replaceAjaxCallback(array $form, FormStateInterface $form_state) {
    return $form['settings'];
  }

  /**
   * Get the entity display mode options.
   *
   * @param $type
   *   The display type, either form, or view.
   *
   * @return array
   *   An array of entity display mode options per type.
   */
  protected function getEntityDisplayModeOptions($type) {
    $options = [];
    $entity_type_id = $this->getEntity()->getEntityTypeId();

    switch ($type) {
      case 'form':
        $options = $this->getEntityDisplay()->getFormModeOptions(
          $entity_type_id
        );
        break;
      case 'view':
        $options = $this->getEntityDisplay()->getViewModeOptions(
          $entity_type_id
        );
        break;
    }

    return $options;
  }

  /**
   * Get the form state value.
   *
   * @param $key
   *   The element key.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   * @param null $default_value
   *   The default value if nothing is found.
   *
   * @return mixed|null
   *   The element key value; otherwise the default value.
   */
  protected function getFormStateValue($key, FormStateInterface $form_state, $default_value = NULL) {
    $parents = !is_array($key) ? [$key] : $key;

    $inputs = [
      $form_state->getValue('settings', []),
      $this->getSettings()
    ];

    foreach ($inputs as $input) {
      $exist = NULL;
      $value = NestedArray::getValue($input, $parents, $exist);
      if ($exist) {
        return $value;
      }
    }

    return $default_value;
  }

  /**
   * Get entity display repository.
   *
   * @return \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   *   A entity display service.
   */
  protected function getEntityDisplay() {
    return \Drupal::service('entity_display.repository');
  }
}
