<?php

namespace Drupal\entity_display_mode\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Define the entity display mode select widget.
 *
 * @FieldWidget(
 *   id = "entity_display_mode_select",
 *   label = @Translation("Select"),
 *   description = @Translation("Select an entity view mode."),
 *   field_types = {
 *     "entity_display_mode"
 *   }
 * )
 */
class EntityDisplayModeSelectWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = !$items[$delta]->isEmpty() ? $items[$delta] : NULL;

    $elements['value'] = [
      '#type' => 'select',
      '#options' =>  $this->getDisplayModeTypeOptions(
        $items->getEntity()
      ),
    ] + $element;

    if ($item instanceof FieldItemInterface) {
      $elements['value']['#default_value'] = $item->get('value')->getValue() ?? NULL;
    }

    return $elements;
  }

  /**
   * Get entity display mode type options.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The field entity instance.
   *
   * @return array
   *   An array of display mode type options.
   */
  protected function getDisplayModeTypeOptions(EntityInterface $entity) {
    $display_type = $this->getFieldSetting('display_type');

    if (!isset($display_type)) {
      return [];
    }
    $options = $this->getEntityDisplayModeOptions(
      $display_type, $entity
    );
    $display_modes = array_filter($this->getFieldSetting('display_mode'));

    if (empty($display_modes)) {
      return $options;
    }

    return array_intersect_key($options, $display_modes);
  }

  /**
   * Get the entity display mode options.
   *
   * @param $type
   *   The display type, either form, or view.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The field entity instance.
   *
   * @return array
   *   An array of entity display mode options per type.
   */
  protected function getEntityDisplayModeOptions($type, EntityInterface $entity) {
    $options = [];
    $entity_type_id = $entity->getEntityTypeId();

    switch ($type) {
      case 'form':
        $options = $this->getEntityDisplay()->getFormModeOptions(
          $entity_type_id
        );
        break;
      case 'view':
        $options = $this->getEntityDisplay()->getViewModeOptions(
          $entity_type_id
        );
        break;
    }

    return $options;
  }

  /**
   * Get entity display repository.
   *
   * @return \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   *   A entity display service.
   */
  protected function getEntityDisplay() {
    return \Drupal::service('entity_display.repository');
  }
}
