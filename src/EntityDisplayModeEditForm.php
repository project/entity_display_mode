<?php

namespace Drupal\entity_display_mode;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\Entity\FieldConfig;

/**
 * Define the entity display mode edit form service.
 */
class EntityDisplayModeEditForm {

  use StringTranslationTrait;

  /***
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructor for the display mode info.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Attach the entity display mode edit form.
   *
   * @param array &$form
   *   An array of form elements.
   * @param FormStateInterface $form_state
   *   An form state instance.
   */
  public function attachForm(&$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $entity */
    $entity = $form_state->getFormObject()->getEntity();

    if (!$entity instanceof ConfigEntityInterface) {
      return;
    }
    $configuration = $this->getEntityDisplayModeConfiguration($entity);

    $form['display']['entity_display_mode'] = [
      '#type' => 'details',
      '#title' => t('Display Mode'),
      '#tree' => TRUE,
      '#open' => TRUE,
    ];
    $form['display']['entity_display_mode']['field_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Display Mode'),
      '#description' => $this->t('Select the field that should be used to 
        dictate what entity mode to use when rendering.'),
      '#options' => $this->getEntityBundleFieldOptions(
        $entity, ['entity_display_mode']
      ),
      '#empty_option' => t('-- Default --'),
      '#default_value' => $configuration['field_name'],
    ];

    $form['#entity_builders'][] = [$this, 'entityEditFormBuilder'];
  }

  /**
   * Entity form builder callback for entity edit form.
   *
   * @param $entity_type
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The configuration entity instance.
   * @param $form
   *   An array of the form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   */
  public function entityEditFormBuilder(
    $entity_type,
    ConfigEntityInterface $entity,
    &$form,
    FormStateInterface $form_state
  ) {
    $entity->setThirdPartySetting(
      'entity_display_mode',
      'display_configuration',
      $form_state->getValue('entity_display_mode')
    );
  }

  /**
   * Default entity display settings.
   *
   * @return array
   *   An array of entity display settings.
   */
  protected function defaultEntityDisplaySettings() {
    return [
      'field_name' => NULL,
    ];
  }

  /**
   * Get entity display mode configuration.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *
   * @return array
   *   An array of the entity display mode configurations.
   */
  protected function getEntityDisplayModeConfiguration(
    ConfigEntityInterface $entity
  ) {
    return $entity->getThirdPartySetting(
      'entity_display_mode',
      'display_configuration',
      $this->defaultEntityDisplaySettings()
    );
  }

  /**
   * Get the entity bundle field options.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The field entity instance.
   * @param array $types
   *   An array of allowed field types.
   *
   * @return array
   *   An array of the entity field options.
   */
  protected function getEntityBundleFieldOptions(
    ConfigEntityInterface $entity,
    array $types = []) {
    $options = [];

    $entity_type = $entity->getEntityType();
    $definitions = $this->entityFieldManager->getFieldDefinitions(
      $entity_type->getBundleOf(), $entity->id()
    );

    foreach ($definitions as $field_name => $definition) {
      if (!$definition instanceof FieldConfig) {
        continue;
      }
      $type = $definition->getType();

      if (!empty($types) && !in_array($type, $types)) {
        continue;
      }
      $options[$field_name] = $definition->getLabel();
    }

    return $options;
  }
}
