Entity Display Mode
===========

The entity display mode module allows site administrators to add a field to collect either a view or form display mode. If the view mode has been selected in the field type, any content that references that view mode on the entity edit form will render the entity using that display mode. 

Installation
------------

* Normal module installation procedure. See
  https://www.drupal.org/documentation/install/modules-themes/modules-8
  
Initial Setup
------------

After the installation, you'll be able to define an entity display mode field for any fieldable entity. 

* Navigate to the content-types listing page (e.g.  `/admin/structure/types`).

* Click `Manage Field` on the content type that you would like to add the display mode field.

* Select the `Display Mode` field from the dropdown.

* Configure the display mode field based on your requirements. In the settings you'll be able to filter what view/form modes are exposed, etc.

* After you've have added a `Display Mode` field, you'll need to navigate back to the content type edit form (e.g. `/admin/structure/types/manage/page`). 

* Once on the entity edit form you'll need scroll to the `Display settings` in the vertical tabs at the bottom. Once in the display settings section, select the field that should be used to dictate what view mode to use when rendering the content of this type.

